<?php

namespace TicketBundle\Service;

use BaseBundle\Entity\Ticket;
use BaseBundle\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class TicketActionService
{

    /** @const string */
    const ID = 'ticket_action_service';

    /** @var  EntityManagerInterface */
    protected $entityManager;

    /**
     * CreateOrderApiService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param $ticketId
     *
     * @return array
     *
     * @throws \Exception
     */
    public function cancelTicketAction($ticketId)
    {
        /** @var TicketRepository $ticketRepository */
        $ticketRepository = $this->entityManager->getRepository(Ticket::class);
        $ticket = $ticketRepository->find($ticketId);
        if (($ticket instanceof Ticket) && ($ticket->getCounter() == 1)) {
            $counter = $ticket->getCounter() + 1;
            $newTicket = new Ticket();
            $ticketNo = $ticket->getTicketNo() + 1;
            $newTicket->setUser($ticket->getUser())
                ->setCustomerOrder($ticket->getCustomerOrder())
                ->setStatus($ticket->getStatus())
                ->setTicketNo($ticketNo)
                ->setCreated(new \DateTime());
            $ticket->setStatus(Ticket::STATUS_CANCELED)
                ->setCounter($counter);
            $this->entityManager->persist($newTicket);
            $this->entityManager->flush();
            return [
                'succes' => $newTicket
            ];
        } else {
            throw new \Exception('Tiket is not valid for close');
        }
    }

    /**
     * @param int $ticketId
     * @param int $status
     *
     * @throws \Exception
     */
    public function updateTicket(int $ticketId, int $status)
    {
        /** @var TicketRepository $ticketRepository */
        $ticketRepository = $this->entityManager->getRepository(Ticket::class);
        $ticket = $ticketRepository->find($ticketId);

        if (!$ticket instanceof Ticket){
            throw new \Exception('Ticket not found');
        }

        /** @var \DateInterval $processingTime */
        $processingTime = $ticket->getStart()->diff($ticket->getStop());

        $ticket->setStop(new \DateTime('now'));
        $ticket->setProcessingTime($processingTime->format('%i'));
        $ticket->setStatus($status);

        $this->entityManager->flush();
    }

}