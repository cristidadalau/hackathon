<?php

namespace TicketBundle\Service;

use BaseBundle\Entity\CustomerOrder;
use BaseBundle\Entity\ProcessingTime;
use BaseBundle\Entity\Ticket;
use BaseBundle\Entity\User;
use BaseBundle\Repository\ProcessingTimeRepository;
use BaseBundle\Repository\TicketRepository;
use BaseBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class TicketProcessingTimeService
 * @package TicketBundle\Service
 */
class TicketProcessingTimeService
{
    /** @var  EntityManagerInterface */
    protected $entityManager;

    /**
     * TicketProcessingTimeService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @param CustomerOrder $customerOrder
     * @return int
     */
    public function calculateOrderDeliveryTimeByUser(User $user, CustomerOrder $customerOrder): int
    {
        /** @var ProcessingTimeRepository $processingTimeRepository */
        $processingTimeRepository = $this->entityManager->getRepository(ProcessingTime::class);

        $orderProcessingTimeByUser = 0;

        foreach ($customerOrder->getProducts() as $product) {
            /** @var ProcessingTime $categoryProcessingTime */
            $categoryProcessingTime = $processingTimeRepository->findOneBy(
                array('category' => $product->getProduct()->getCategory(), 'user' => $user));

            $orderProcessingTimeByUser += $categoryProcessingTime->getProcessingTime() * $product->getQuantity();
        }

        return $orderProcessingTimeByUser;
    }

    /**
     * @param $user
     * @return int
     */
    public function calculateBacklogTimeByUser($user)
    {
        /** @var TicketRepository $ticketRepository */
        $ticketRepository = $this->entityManager->getRepository(Ticket::class);

        return (int)$ticketRepository->getUserBacklogTime($user);
    }

    /**
     * @param CustomerOrder $customerOrder
     * @return array
     */
    public function calculateFastestUserProcessingTime(CustomerOrder $customerOrder): array
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);

        $activeUsers = $userRepository->findBy(array('status' => User::STATUS_ACTIVE));

        $fastestUser = [];

        /** @var User $user */
        foreach ($activeUsers as $user) {
            $currentUserOrderTime = $this->calculateOrderDeliveryTimeByUser($user, $customerOrder);
            $currentUserBacklogTime = $this->calculateBacklogTimeByUser($user);

            $totalUserProcessingTime = $currentUserOrderTime + $currentUserBacklogTime;

            if ($fastestUser['totalProcessingTime'] > $totalUserProcessingTime) {
                $fastestUser = [
                    'user' => $user,
                    'currentOrderProcessingTime' => $currentUserOrderTime,
                    'totalProcessingTime' => $totalUserProcessingTime
                ];
            }
        }

        return $fastestUser;
    }
}