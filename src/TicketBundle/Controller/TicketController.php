<?php

namespace TicketBundle\Controller;

use BaseBundle\Entity\Ticket;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use TicketBundle\Service\TicketActionService;

class TicketController extends Controller
{
    /**
     * @Route("/ticket")
     */
    public function indexAction()
    {
        return $this->render('@Ticket/index.html.twig');
    }

    /**
     * @Route("/cancel-ticket/{ticketId}")
     *
     * @param $ticketId
     */
    public function cancelAction($ticketId)
    {
        try {
            $this->get(TicketActionService::class)->cancelTicketAction($ticketId);
            return new JsonResponse([
                'error' => false,
                'message' => 'succes'
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/update-ticket/{ticketId}/{status}")
     *
     * @Method({"GET"})
     *
     * @param int $ticketId
     * @param int $status
     *
     * @return JsonResponse
     */
    public function updateTicketAction(int $ticketId, int $status)
    {
        try {

            $data = $this->get(TicketActionService::class)->updateTicket($ticketId, $status);

            return new JsonResponse([
                'error' => false,
                'data' => $data
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'error' => true,
                'message' => $e->getMessage() . $e->getTraceAsString()
            ]);
        }
    }
}
