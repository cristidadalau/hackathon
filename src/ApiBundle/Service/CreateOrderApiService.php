<?php

namespace ApiBundle\Service;

use BaseBundle\Entity\Customer;
use BaseBundle\Entity\CustomerOrder;
use BaseBundle\Entity\CustomerOrderProduct;
use BaseBundle\Entity\Product;
use BaseBundle\Entity\Ticket;
use BaseBundle\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use TicketBundle\Service\TicketProcessingTimeService;

class CreateOrderApiService
{
    /** @const string */
    const CREATE_ORDER_METHOD = '/api/create-order';

    /** @var  EntityManagerInterface */
    protected $entityManager;

    /** @var  TicketProcessingTimeService */
    protected $ticketProcessingTimeService;

    /**
     * CreateOrderApiService constructor.
     * @param EntityManagerInterface $entityManager
     * @param TicketProcessingTimeService $ticketProcessingTimeService
     */
    public function __construct(EntityManagerInterface $entityManager, TicketProcessingTimeService $ticketProcessingTimeService)
    {
        $this->entityManager = $entityManager;
        $this->ticketProcessingTimeService = $ticketProcessingTimeService;
    }

    /**
     * @param $orderData
     */
    public function createOrder($orderData)
    {
        $customerOrder = new CustomerOrder();

        $customerOrder->setCustomer($this->entityManager
            ->getReference(Customer::class, $orderData['customer_id']));
        $customerOrder->setVerifyProduct($orderData['verify_product']);
        $customerOrder->setPrintInvoice($orderData['print_invoice']);
        $customerOrder->setOrderTotal(0);
        $customerOrder->setStatus(CustomerOrder::STATUS_FINALIZED);

        $this->entityManager->persist($customerOrder);

        $orderTotal =  0;
        foreach ($orderData['products'] as $product){
            $orderTotal  += $product['price'];
            $orderProduct = new CustomerOrderProduct();

            $orderProduct->setPrice($product['price']);
            $orderProduct->setCustomerOrder($customerOrder);
            $orderProduct->setProduct($this->entityManager
                ->getReference(Product::class, $product['id']));

            $this->entityManager->persist($orderProduct);
        }
        $customerOrder->setOrderTotal($orderTotal);

        $this->createNewTicket($customerOrder);

         $this->entityManager->flush();

        //send user notification - minus x minutes
    }

    /**
     * @param CustomerOrder $customerOrder
     */
    public function createNewTicket(CustomerOrder $customerOrder)
    {
        /** @var TicketRepository $ticketRepository */
        $ticketRepository = $this->entityManager->getRepository(Ticket::class);

        /** @var Ticket $lastTicket */
        $lastTicket = $ticketRepository->findOneBy(array(), array('ticketNo' => 'DESC'));

        $ticketNo = $lastTicket->getTicketNo() + 1;

        $fastestUserProcessingTime = $this->ticketProcessingTimeService
            ->calculateFastestUserProcessingTime($customerOrder);

        /** @var Ticket $ticket */
        $ticket = new Ticket();

        $ticket->setCustomerOrder($customerOrder)
            ->setTicketNo($ticketNo)
            ->setUser($fastestUserProcessingTime['user'])
            ->setProcessingTime($fastestUserProcessingTime['currentOrderProcessingTime']);

        $this->entityManager->persist($ticket);
    }
}