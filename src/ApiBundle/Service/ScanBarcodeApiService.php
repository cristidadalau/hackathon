<?php

namespace ApiBundle\Service;

use BaseBundle\Entity\Customer;
use BaseBundle\Entity\Product;
use BaseBundle\Repository\CustomerRepository;
use BaseBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PromotionBundle\Service\PromotionService;

class ScanBarcodeApiService
{
    /** @const string */
    const SCAN_BARCODE_METHOD = '/api/scan-barcode';

    /** @var  EntityManagerInterface */
    protected $entityManager;

    /** @var  PromotionService */
    protected $promoService;

    /**
     * ScanBarcodeApiService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, PromotionService $promotionService)
    {
        $this->entityManager = $entityManager;
        $this->promoService = $promotionService;
    }

    public function scanBarcode($barcode,$userId)
    {
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $this->entityManager->getRepository(Customer::class);
        $customer = $customerRepository->find($userId);

        if (!$customer instanceof Customer) {
            throw new \Exception('No customer found');
        }

        /** @var ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);

        /** @var Product $product */
        $product = $productRepository->getProductFromBarcode($barcode);

        if (!$product instanceof Product) {
            throw new \Exception('No product found');
        }


        $products[] = $product->toApiResponse();
        $discountValue = $this->promoService->computePromotionForProduct($product, $customer);
        if ($discountValue < 0) {
            $products[] = [
                'id' => 0,
                'parentId' => $product->getId(),
                'productName' => 'DISCOUNT',
                'productPrice' => $discountValue,
                'type' => 'promo',
                'barcode' => '',
                'imageUrl' => ''
            ];
        }

        return [
            'products' => $products
        ];
    }
}