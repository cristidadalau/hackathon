<?php

namespace ApiBundle\Controller;

use ApiBundle\Service\CreateOrderApiService;
use ApiBundle\Service\ScanBarcodeApiService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class ApiController extends Controller
{
    /**
     * @Route("/api/scan-barcode/{scanBarcode}/{userId}")
     * @Method({"GET"})
     *
     * @param $scanBarcode
     * @param $userId
     *
     * @return JsonResponse;
     *
     */
    public function scanBarcodeAction($scanBarcode,$userId): JsonResponse
    {
        try {

            $data = $this->get(ScanBarcodeApiService::class)->scanBarcode($scanBarcode,$userId);

            return  new JsonResponse([
                'error' => false,
                'data' => $data
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/api/create-order")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createOrderAction(Request $request)
    {
        try{

            $params = $request->request->all();

            $data = $this->get(CreateOrderApiService::class)->createOrder($params);

            return new JsonResponse([
                'error' => false,
                'data' => $data
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'error' => true,
                'message' => $e->getMessage().$e->getTraceAsString()
            ]);
        }
    }
}