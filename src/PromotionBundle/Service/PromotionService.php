<?php

namespace PromotionBundle\Service;

use BaseBundle\Entity\CategoryDiscount;
use BaseBundle\Entity\Customer;
use BaseBundle\Entity\CustomerOrder;
use BaseBundle\Entity\OrderDiscount;
use BaseBundle\Entity\Product;
use BaseBundle\Repository\CategoryDiscountRepository;
use BaseBundle\Repository\CustomerOrderRepository;
use BaseBundle\Repository\OrderDiscountRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PromotionService
 * @package PromotionBundle\Service
 */
class PromotionService
{
    const ORDER_TOTAL_PERCENTAGE = 75;
    const CATEGORY_TOTAL_PERCENTAGE = 25;

    /** @var  EntityManagerInterface */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Customer $customer
     * @param float $productPrice
     * @return float
     */
    protected function computeDiscountByOrderTotal(Customer $customer, float $productPrice): float
    {
        $orderTotal = $this->getOrderTotalForCustomer($customer);

        /** @var OrderDiscountRepository $orderDiscountRepository */
        $orderDiscountRepository = $this->entityManager->getRepository(OrderDiscount::Class);

        /** @var OrderDiscount $orderDiscount */
        $orderDiscount = $orderDiscountRepository->getOrderDiscountByOrderTotal($orderTotal);

        return (($orderDiscount instanceof OrderDiscount) ? $orderDiscount->getOrderDiscountPercent() : 0) * $productPrice/100;

    }

    /**
     * @param Customer $customer
     * @param $categoryId
     * @param float $productPrice
     * @return float
     */
    protected function computeDiscountByCategoryTotal(Customer $customer, $categoryId, float $productPrice): float
    {
        $categoryTotal = $this->getCategoryTotalForCustomer($customer, $categoryId);

        /** @var CategoryDiscountRepository $categoryDiscountRepository */
        $categoryDiscountRepository = $this->entityManager->getRepository(CategoryDiscount::Class);

        /** @var CategoryDiscount $categoryDiscount */
        $categoryDiscount = $categoryDiscountRepository->getDiscountByCategoryTotal($categoryId, $categoryTotal);

        return (($categoryDiscount instanceof OrderDiscount) ? $categoryDiscount->getCategoryDiscountPercent() : 0) * $productPrice;
    }

    /**
     * @param Customer $customer
     *
     * @return float
     */
    protected function getOrderTotalForCustomer(Customer $customer): float
    {
        /** @var CustomerOrderRepository $customerOrderRepository */
        $customerOrderRepository = $this->entityManager->getRepository(CustomerOrder::class);

        $customerOrders = $customerOrderRepository->getCustomerActiveOrders($customer);
        $orderTotal = 0;
        /** @var CustomerOrder $customerOrder */
        foreach ($customerOrders as $customerOrder) {
            $orderTotal += $customerOrder->getOrderTotal();
        }

        return $orderTotal;
    }

    /**
     * @param Customer $customer
     * @param $categoryId
     * @return float
     */
    protected function getCategoryTotalForCustomer(Customer $customer, $categoryId): float
    {
        /** @var CustomerOrderRepository $customerOrderRepository */
        $customerOrderRepository = $this->entityManager->getRepository(CustomerOrder::class);

        $categoryTotal = $customerOrderRepository->getCategoryTotalByCustomer($customer, $categoryId);

        return (float)$categoryTotal;
    }

    /**
     * @param Product $product
     * @param Customer $customer
     *
     * @return float|string
     */
    public function computePromotionForProduct(Product $product, Customer $customer)
    {
        $discountForOrderTotal = $this->computeDiscountByOrderTotal($customer, $product->getProductPrice());
        $discountForCategoryTotal = $this->computeDiscountByCategoryTotal($customer, $product->getCategory()->getId(), $product->getProductPrice());

        $promotionTotal = $discountForOrderTotal * static::ORDER_TOTAL_PERCENTAGE / 100 + $discountForCategoryTotal * static::CATEGORY_TOTAL_PERCENTAGE / 100;

        return -1 * $promotionTotal;
    }

}