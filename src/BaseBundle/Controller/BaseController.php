<?php

namespace BaseBundle\Controller;

use PromotionBundle\Service\PromotionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaseController extends Controller
{
    /**
     * @Route("/test")
     */
    public function indexAction()
    {

        $promotService = $this->get(PromotionService::class);
        return $this->render('@Base/index.html.twig');
    }

}
