<?php

namespace BaseBundle\Repository;
use BaseBundle\Entity\OrderDiscount;

/**
 * OrderDiscountRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrderDiscountRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param float $orderTotal
     */
    public function getOrderDiscountByOrderTotal(float $orderTotal)
    {
        return $this->createQueryBuilder('od')
            ->where(':orderTotal >= od.orderLevelMin and :orderTotal < od.orderLevelMax')
            ->setParameter('orderTotal', $orderTotal)
            ->andWhere('od.status <> :canceledStatus')
            ->setParameter('canceledStatus', OrderDiscount::INACTIVE)
            ->getQuery()->getOneOrNullResult();
    }


}
