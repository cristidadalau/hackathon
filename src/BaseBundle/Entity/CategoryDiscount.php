<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryDiscount
 *
 * @ORM\Table(name="category_discount", indexes={@ORM\Index(name="category_id", columns={"category_id"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\CategoryDiscountRepository")
 */
class CategoryDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_level_min", type="smallint", nullable=false)
     */
    private $categoryLevelMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_level_max", type="smallint", nullable=false)
     */
    private $categoryLevelMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_discount_percent", type="smallint", nullable=false)
     */
    private $categoryDiscountPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = 'CURRENT_TIMESTAMP';



    /**
     * Set category
     *
     * @param Category $category
     *
     * @return CategoryDiscount
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set categoryLevelMin
     *
     * @param integer $categoryLevelMin
     *
     * @return CategoryDiscount
     */
    public function setCategoryLevelMin($categoryLevelMin)
    {
        $this->categoryLevelMin = $categoryLevelMin;

        return $this;
    }

    /**
     * Get categoryLevelMin
     *
     * @return integer
     */
    public function getCategoryLevelMin()
    {
        return $this->categoryLevelMin;
    }

    /**
     * Set categoryLevelMax
     *
     * @param integer $categoryLevelMax
     *
     * @return CategoryDiscount
     */
    public function setCategoryLevelMax($categoryLevelMax)
    {
        $this->categoryLevelMax = $categoryLevelMax;

        return $this;
    }

    /**
     * Get categoryLevelMax
     *
     * @return integer
     */
    public function getCategoryLevelMax()
    {
        return $this->categoryLevelMax;
    }

    /**
     * Set categoryDiscountPercent
     *
     * @param integer $categoryDiscountPercent
     *
     * @return CategoryDiscount
     */
    public function setCategoryDiscountPercent($categoryDiscountPercent)
    {
        $this->categoryDiscountPercent = $categoryDiscountPercent;

        return $this;
    }

    /**
     * Get categoryDiscountPercent
     *
     * @return integer
     */
    public function getCategoryDiscountPercent()
    {
        return $this->categoryDiscountPercent;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return CategoryDiscount
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return CategoryDiscount
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return CategoryDiscount
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
