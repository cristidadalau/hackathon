<?php

namespace BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CustomerOrder
 *
 * @ORM\Table(name="customer_order")
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\CustomerOrderRepository")
 */
class CustomerOrder
{
    const STATUS_CANCELED = 0;
    const STATUS_FINALIZED = 2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CustomerOrderProduct[]|ArrayCollection
     *@ORM\OneToMany(
     *     targetEntity="BaseBundle\Entity\CustomerOrderProduct",
     *     mappedBy="customerOrder",
     *     indexBy="id",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="id", referencedColumnName="order_id")
     */
    private $products;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Customer", inversedBy="customerOrders")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var Ticket
     * @ORM\OneToOne(
     *     targetEntity="Ticket",
     *     mappedBy="customerOrder",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     */
    private $ticket;

    /**
     * @var float
     *
     * @ORM\Column(name="order_total", type="float", nullable=false)
     */
    private $orderTotal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="verify_product", type="boolean", nullable=false)
     */
    private $verifyProduct;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_invoice", type="boolean", nullable=false)
     */
    private $printInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;


    /**
     * CustomerOrder constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set products
     *
     * @param Product[]|ArrayCollection $products
     *
     * @return CustomerOrder
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get products
     *
     * @return CustomerOrderProduct[]|ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return CustomerOrder
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return float
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * @param float $orderTotal
     * @return CustomerOrder
     */
    public function setOrderTotal($orderTotal)
    {
        $this->orderTotal = $orderTotal;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVerifyProduct(): bool
    {
        return $this->verifyProduct;
    }

    /**
     * @param bool $verifyProduct
     * @return CustomerOrder
     */
    public function setVerifyProduct(bool $verifyProduct): CustomerOrder
    {
        $this->verifyProduct = $verifyProduct;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPrintInvoice(): bool
    {
        return $this->printInvoice;
    }

    /**
     * @param bool $printInvoice
     * @return CustomerOrder
     */
    public function setPrintInvoice(bool $printInvoice): CustomerOrder
    {
        $this->printInvoice = $printInvoice;
        return $this;
    }


    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return CustomerOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Ticket
     */
    public function getTicket(): Ticket
    {
        return $this->ticket;
    }

    /**
     * @param Ticket $ticket
     */
    public function setTicket(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }
}

