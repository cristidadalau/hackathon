<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomerOrderProduct
 *
 * @ORM\Table(name="customer_order_products")
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\CustomerOrderProductRepository")
 */
class CustomerOrderProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CustomerOrder
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\CustomerOrder")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $customerOrder;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=0)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=false)
     */
    private $quantity;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CustomerOrder
     */
    public function getCustomerOrder()
    {
        return $this->customerOrder;
    }

    /**
     * @param CustomerOrder $customerOrder
     * @return CustomerOrderProduct
     */
    public function setCustomerOrder($customerOrder)
    {
        $this->customerOrder = $customerOrder;
        return $this;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return CustomerOrderProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return CustomerOrderProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return CustomerOrderProduct
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }
}

