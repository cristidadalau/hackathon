<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket", indexes={@ORM\Index(name="ticket_no", columns={"ticket_no"}), @ORM\Index(name="order_id", columns={"order_id"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\TicketRepository")
 */
class Ticket
{
    /** @const int */
    const STATUS_NEW = 1;

    /** @const int */
    const STATUS_PREPARED =2;

    /** @const int */
    const STATUS_CLOSED = 3;

    /** @const int */
    const STATUS_CANCELED = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CustomerOrder
     *
     * @ORM\OneToOne(targetEntity="CustomerOrder", inversedBy="ticket")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $customerOrder;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_no", type="smallint", nullable=false)
     */
    private $ticketNo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stop", type="datetime", nullable=true)
     */
    private $stop;

    /**
     * @var float
     *
     * @ORM\Column(name="processing_time", type="smallint", nullable=true)
     */
    private $processingTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="counter", type="smallint", nullable=false)
     */
    private $counter = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Set customerOrder
     *
     * @param CustomerOrder $customerOrder
     *
     * @return Ticket
     */
    public function setCustomerOrder($customerOrder)
    {
        $this->customerOrder = $customerOrder;

        return $this;
    }

    /**
     * Get customerOrder
     *
     * @return CustomerOrder
     */
    public function getCustomerOrder()
    {
        return $this->customerOrder;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Ticket
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ticketNo
     *
     * @param integer $ticketNo
     *
     * @return Ticket
     */
    public function setTicketNo($ticketNo)
    {
        $this->ticketNo = $ticketNo;

        return $this;
    }

    /**
     * Get ticketNo
     *
     * @return integer
     */
    public function getTicketNo()
    {
        return $this->ticketNo;
    }

    /**
     * @return \DateTime
     */
    public function getStart(): \DateTime
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     * @return Ticket
     */
    public function setStart(\DateTime $start): Ticket
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStop(): \DateTime
    {
        return $this->stop;
    }

    /**
     * @param \DateTime $stop
     * @return Ticket
     */
    public function setStop(\DateTime $stop): Ticket
    {
        $this->stop = $stop;
        return $this;
    }

    /**
     * @return float
     */
    public function getProcessingTime(): float
    {
        return $this->processingTime;
    }

    /**
     * @param float $processingTime
     * @return Ticket
     */
    public function setProcessingTime(float $processingTime): Ticket
    {
        $this->processingTime = $processingTime;
        return $this;
    }


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Ticket
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Ticket
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param integer $counter
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;
    }


}
