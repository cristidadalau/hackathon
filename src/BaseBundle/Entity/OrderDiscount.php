<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDiscount
 *
 * @ORM\Table(name="order_discount", indexes={@ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\OrderDiscountRepository")
 */
class OrderDiscount
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_level_min", type="integer", nullable=false)
     */
    private $orderLevelMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_level_max", type="integer", nullable=false)
     */
    private $orderLevelMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_discount_percent", type="smallint", nullable=false)
     */
    private $orderDiscountPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = 'CURRENT_TIMESTAMP';



    /**
     * Set orderLevelMin
     *
     * @param integer $orderLevelMin
     *
     * @return OrderDiscount
     */
    public function setOrderLevelMin($orderLevelMin)
    {
        $this->orderLevelMin = $orderLevelMin;

        return $this;
    }

    /**
     * Get orderLevelMin
     *
     * @return integer
     */
    public function getOrderLevelMin()
    {
        return $this->orderLevelMin;
    }

    /**
     * Set orderLevelMax
     *
     * @param integer $orderLevelMax
     *
     * @return OrderDiscount
     */
    public function setOrderLevelMax($orderLevelMax)
    {
        $this->orderLevelMax = $orderLevelMax;

        return $this;
    }

    /**
     * Get orderLevelMax
     *
     * @return integer
     */
    public function getOrderLevelMax()
    {
        return $this->orderLevelMax;
    }

    /**
     * Set orderDiscountPercent
     *
     * @param integer $orderDiscountPercent
     *
     * @return OrderDiscount
     */
    public function setOrderDiscountPercent($orderDiscountPercent)
    {
        $this->orderDiscountPercent = $orderDiscountPercent;

        return $this;
    }

    /**
     * Get orderDiscountPercent
     *
     * @return integer
     */
    public function getOrderDiscountPercent()
    {
        return $this->orderDiscountPercent;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return OrderDiscount
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return OrderDiscount
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return OrderDiscount
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
